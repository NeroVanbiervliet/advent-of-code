#!/usr/bin/env bb

(def INPUT_FILE "input.txt")
(def SIMULATION_STEPS 110)

(defn fish-to-next-fish
      [number]
      (cond
        (= number 0) '(6 8)
        :else (list (- number 1))))

(defn get-input
      []
      (->>
        INPUT_FILE
        io/file
        slurp
        (#(str/split % #","))
        (map #(Integer/parseInt %))))

(defn -main
      []
      (loop [population (get-input) step 0]
          (when (<= step SIMULATION_STEPS)
                ;(println population)
                (println step ":" (count population))
                (recur
                       (->> population (map fish-to-next-fish) flatten)
                       (+ step 1)))))

(-main)