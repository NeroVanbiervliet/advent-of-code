#!/usr/bin/env joker

(def INPUT_FILE "input.txt")

(defn parse-int [str] (joker.strconv/parse-int str 10 64)) ; base 10, 64 bit

(defn translate [[command value-str]]
      (let [value (parse-int value-str)]
        (case command
              "forward" ["forward" value]
              "up" ["depth" (* -1 value)]
              "down" ["depth" value])))

(defn calc-sum [command command-seq]
      (->> command-seq
           (filter #(= command (first %)))
           (map last)
           (reduce +)))

(defn -main
      []
      (->>
        (joker.os/open INPUT_FILE)
        line-seq
        (map #(joker.string/split % " "))
        (map translate)
        (#(* (calc-sum "depth" %) (calc-sum "forward" %)))
        print))

(-main)