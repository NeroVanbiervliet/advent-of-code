#!/usr/bin/env joker

(def INPUT_FILE "input.txt")

(defn parse-int [str] (joker.strconv/parse-int str 10 64)) ; base 10, 64 bit

(defn -main
      []
      (->>
        (joker.os/open INPUT_FILE)
        line-seq
        (map parse-int)
        ((fn [coll] (map  - (rest coll) coll))) ; can i write this better?
        (filter #(> % 0))
        count
        print))

(-main)