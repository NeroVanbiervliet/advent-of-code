#!/usr/bin/env bb

(def INPUT_FILE "input.txt")

(defn get-input
      []
      (->>
        INPUT_FILE
        io/file
        slurp
        (#(str/split % #","))
        (map #(Integer/parseInt %))))

; from https://rosettacode.org/wiki/Averages/Median
(defn median [ns]
      (let [ns (sort ns)
            cnt (count ns)
            mid (bit-shift-right cnt 1)]
           (if (odd? cnt)
             (nth ns mid)
             (/ (+ (nth ns mid) (nth ns (dec mid))) 2))))

(defn -main []
     (->>
       (let [pos (get-input) med (median pos)]
            (->>
              pos
              (map #(- % med))
              (map #(Math/abs %))
              (reduce +)
              (println)))))


(-main)